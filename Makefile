# Enable delayed variable expansion
.SECONDEXPANSION:
# Disable any existing implicit rule
.SUFFIXES:

all: build

.PHONY: all

define relpath
$(shell realpath -s --relative-to="$1" "$2")
endef

define relpathbuild
$(call relpath,"$(MKS_TOP_BUILDDIR)","$1")
endef

MKS_PROJDIR ?= .
MKS_TOP_BUILDDIR ?= .
MKS := $(patsubst %/,%,$(dir $(lastword $(MAKEFILE_LIST))))
MKS_THIS := $(MKS)/Makefile
MKS_CONFIG := $(MKS_TOP_BUILDDIR)/Makefile.conf
MKS_CACHEDIR := $(MKS_TOP_BUILDDIR)/makesys.cache
MKS_CUR_SUBDIR ?= /
MKS_LOCALPROJDIR := $(MKS_PROJDIR)$(MKS_CUR_SUBDIR)
MKS_LOCALBUILDDIR := $(MKS_TOP_BUILDDIR)$(MKS_CUR_SUBDIR)
MKS_LOCALCACHEDIR := $(MKS_CACHEDIR)$(MKS_CUR_SUBDIR)

empty :=

V ?=
V_ECHO_CMD   := @
V_SILENT_CMD := @
ifneq ("x$(V)","x")
  V_ECHO_CMD   := @true $(empty)
  V_SILENT_CMD :=
endif

define mks_chk_targets_single
$(foreach tgt,$1, \
  $(if $(and $(filter $(tgt),$(MAKECMDGOALS)),$(filter-out $(tgt),$(MAKECMDGOALS))), \
    $(error The target "$(tgt)" cannot be given along with other targets) \
  ) \
)
endef

$(call mks_chk_targets_single,defconfig setup)

ifneq ($(filter defconfig,$(MAKECMDGOALS)),)
  include $(MKS)/Makefile.defconfig
else ifneq ($(filter setup,$(MAKECMDGOALS)),)
  include $(MKS)/Makefile.setup
else
  include $(MKS)/Makefile.generic
endif
