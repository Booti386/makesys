#!/bin/bash

function usage {
	echo "Usage: $0 [project_dir [build_dir]]"
	exit 1
}

MKS_DIR="$(realpath -s "$(dirname "${BASH_SOURCE[0]}")")"
PROJ_DIR="."
BUILD_DIR="."

[ "$#" -gt "2" ] && usage
[ "$1" = "--help" ] && usage

if [ "$#" -ge "1" ]; then
	PROJ_DIR="$1"
fi

if [ "$#" -ge "2" ]; then
	BUILD_DIR="$2"
fi

MKS_DIR="$(realpath -s --relative-to="${BUILD_DIR}" "${MKS_DIR}")"
PROJ_DIR="$(realpath -s --relative-to="${BUILD_DIR}" "${PROJ_DIR}")"

make -C "${BUILD_DIR}" -f "${MKS_DIR}/Makefile" setup MKS_PROJDIR="${PROJ_DIR}"
